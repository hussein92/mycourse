<?php
include_once('../functions/all.php');
is_logged_in();
$cust_set = show_customers(0);
$customerscount=mysqli_num_rows($cust_set);
$pagetitle = "Delete Customer";
include_once("../layout/header.php");
include_once("../layout/navbar.php");
 ?>



<div class="container">
  <h1 class="display-3">Deleted Customers List</h1><?php echo "No of records: ". $customerscount ; ?><br>
    <a href="customers.php" class="btn btn-success mt-2 mb-2 ml-3">Back to Customers</a>
  <table class="table">
    <tr>
      <th>Customer Name</th>
      <th>Customer Phone</th>
      <th>Customer Email</th>
      <th>Customer address</th>
      <th>Customer City</th>
      <th>Actions</th>
    </tr>
    <?php
    while ($customerinfo = mysqli_fetch_assoc($cust_set)) {
     ?>
    <tr>
      <td><?php echo $customerinfo["cname"];?></td>
      <td><?php echo $customerinfo["cphone"];?></td>
      <td><?php echo $customerinfo["cemail"];?></td>
      <td><?php echo $customerinfo["caddress"];?></td>
      <td><?php echo $customerinfo["cityname"];?></td>
      <td>
        <a href="delete_restore_customer.php?cid=<?php echo $customerinfo["cid"];?>&action=1" class="btn btn-success btn-sm">Restore</a></td>
    </tr>
  <?php
  }
  ?>
    </table>

</div>

</body>
</html>
