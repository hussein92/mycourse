<?php
include_once('../functions/all.php');
is_logged_in();
$cust_set = show_customers(1);
$customerscount = mysqli_num_rows($cust_set);
 ?>

<?php
$pagetitle = "Customers List";
include_once('../layout/header.php');
include_once('../layout/navbar.php');
?>

<div class="container">
  <h1 class="display-3">Customers List</h1><br>
  <a href="new_customer.php" class="btn btn-success mt-2 mb-2">Add New</a>
  <a href="customers_deleted.php" class="btn btn-danger mt-2 mb-2 ml-3">Deleted Customers</a>
  <a href="search_by_phone.php" class="btn btn-primary mt-2 mb-2 ml-3">Search By Phone</a>
  <a href="search_by_name.php" class="btn btn-primary mt-2 mb-2 ml-3">Search By name</a>
  <a href="search_by_field.php" class="btn btn-primary mt-2 mb-2 ml-3">Search By Field</a>
  <a href="search_dynamic.php" class="btn btn-primary mt-2 mb-2 ml-3">Dynamic Search</a>
  <br>
    <?php echo "No of records: ". $customerscount ; ?>
</div>
<div class="container-fliud">
  <div class="row">
  <div class="col col-1"></div>
  <div class="col col-10">
  <table class="table">
    <tr>
      <th>Customer Profile</th>
      <th>Customer Name</th>
      <th>Customer Phone</th>
      <th>Customer Email</th>
      <th>Customer address</th>
      <th>Customer City</th>
      <th>Actions</th>
    </tr>
    <?php
    while ($customerinfo = mysqli_fetch_assoc($cust_set)) {
     ?>
    <tr>
      <td> <img src="../img/customers/<?php echo $customerinfo["cprofile"];?>" style="width:50px;" alt=""></td>
      <td><?php echo $customerinfo["cname"];?></td>
      <td><?php echo $customerinfo["cphone"];?></td>
      <td><?php echo $customerinfo["cemail"];?></td>
      <td><?php echo $customerinfo["caddress"];?></td>
      <td><?php echo $customerinfo["cityname"];?></td>
      <td class="w.100">
        <a href="view_customer.php?cid=<?php echo $customerinfo["cid"];?>" class="btn btn-secondary btn-sm">View</a>
        <a href="update_customer.php?cid=<?php echo $customerinfo["cid"];?>" class="btn btn-primary btn-sm">Edit</a>
        <a href="delete_restore_customer.php?cid=<?php echo $customerinfo["cid"];?>&action=0" class="btn btn-danger btn-sm">Delete</a></td>
    </tr>
  <?php
  }
  ?>

    </table>
    </div>
    <div class="col col-1"></div>
</div>
</div>
<?php include "../layout/footer.php"; ?>
