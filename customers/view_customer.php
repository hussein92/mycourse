<?php
include_once("../functions/all.php");
is_logged_in();
$pagetitle = "View Customer profile";

$cid=$_GET["cid"];

$data2 = showCustomer($cid);
$cinfo = mysqli_fetch_assoc($data2);

include_once("../layout/header.php");
include_once("../layout/navbar.php");
 ?>


<div class="container">
  <h1 class="display-3">View Customer Profile</h1>
<?php
// Assocciative array
echo "Customer ID: ". $cinfo["cid"]."<br>";
echo "Customer Name: ". $cinfo["cname"]."<br>";
echo "Customer Email: ". $cinfo["cemail"]."<br>";
echo "Customer Phone: ". $cinfo["cphone"]."<br>";
echo "Customer Address: ". $cinfo["caddress"]."<br>";
echo "Customer City : ". $cinfo["cityname"]."<br>";
 ?>
<br>
 <a href="customers.php" class="btn btn-secondary mt-2 mb-2">Back</a>
</div>


<?php include_once("../layout/footer.php"); ?>