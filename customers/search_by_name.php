<?php
include_once("../functions/all.php");
is_logged_in();
$pagetitle = "Search By Name";

if(isset($_POST["name"])) {
$search=$_POST["name"];
$method ="cname";
$searchType = $_POST["searhType"];

$cust_set = customersSearch($search,$method,$searchType);
$customerscount=mysqli_num_rows($cust_set);
}
include_once("../layout/header.php");
include_once("../layout/navbar.php");
 ?>
 

    <div class="container">
      <h1 class="display-3">Search By Name</h1>
      <form class="form-inline mb-3" action="search_by_name.php" method="post">
        <label for="name">Type Customer Name Here</label>
        <input name="name" type="text" class="form-control ml-2 mr-2" value="<?php if(isset($_POST["name"])){echo $_POST["name"];} ?>">
        <laber id="searchType" for="searchType"> Full / Part </laber>
        <select name="searhType" class="form-control ml-3">
          <option value="full" <?php if(isset($searchType) && $searchType =="full" ){echo "selected";} ?> >Full Match</option>
          <option value="part" <?php if(isset($searchType) && $searchType =="part" ){echo "selected";} ?>>Part Of Word</option>
        </select>
        <button type="submit" class="btn btn-primary ml-2">Search</button>
        <a href="customers.php" class="btn btn-secondary mt-2 mb-2 ml-3">Back</a>
      </form>
      <?php
      if(isset($_POST["name"]) && $customerscount>0){
       ?>
       <?php echo "No of records: ". $customerscount ; ?>
        <table class="table">
            <tr>
              <th>Customer Name</th>
              <th>Customer Phone</th>
              <th>Customer Email</th>
              <th>Customer address</th>
              <th>Customer City</th>
              <th>Actions</th>
            </tr>
          <?php
          while ($customerinfo = mysqli_fetch_assoc($cust_set)) {
           ?>
            <tr>
              <td><?php echo $customerinfo["cname"];?></td>
              <td><?php echo $customerinfo["cphone"];?></td>
              <td><?php echo $customerinfo["cemail"];?></td>
              <td><?php echo $customerinfo["caddress"];?></td>
              <td><?php echo $customerinfo["cityname"];?></td>
              <td>
                <a href="view_customer.php?cid=<?php echo $customerinfo["cid"];?>" class="btn btn-secondary btn-sm">View</a>
                <a href="update_customer.php?cid=<?php echo $customerinfo["cid"];?>" class="btn btn-primary btn-sm">Edit</a>
                <a href="delete_customer.php?cid=<?php echo $customerinfo["cid"];?>" class="btn btn-danger btn-sm">Delete</a></td>
            </tr>
          <?php
          }
          ?>
        </table>
      <?php } elseif (isset($_POST["name"]) && $customerscount==0) {?>
        <h1 class="display-4 text-center text-danger">No records match your criteria</h1>
      <?php } else {?>
        <h1 class="display-4 text-center">No Data to show</h1>
    <?php  } ?>
    </div>

    </body>
    </html>
