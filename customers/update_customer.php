<?php
include_once("../functions/all.php");
is_logged_in();
$pagetitle = "Update Customer";

if(isset($_POST["cid"])){
  $cid=$_POST["cid"];
  $cname=$_POST["cname"];
  $cphone=$_POST["cphone"];
  $cemail=$_POST["cemail"];
  $caddress=$_POST["caddress"];
  $ccityid=$_POST["ccityid"];

  if($_FILES["cprofile"]["size"]>0)
  {
    $cphoto=$_FILES["cprofile"]["name"];
    $path="../img/customers/";
    $file=$_FILES["cprofile"]["tmp_name"];
    $photoQuery = ",cprofile='".$cphoto."'";
    move_uploaded_file($file,$path.$cphoto); 
  }
  else{$photoQuery ="";}
  
  $customerupd= updateCustomer($photoQuery,$cname,$cphone,$cemail,$caddress,$ccityid,$cid);
 
}

$cid=$_GET["cid"];
$data = showCustomer($cid);
$customerinfo = mysqli_fetch_assoc($data);


$cities=showCities();
include_once("../layout/header.php");
include_once("../layout/navbar.php");
 ?>
 
<div class="container">

    <table>
      <h1 class="display-3">Update Customer</h1>
      <form action="update_customer.php" method="post" enctype="multipart/form-data">
    	<div class="form-group ">
          <label for="cprofile" class="mx-2">Profile Picture:</label>
          <img src="../img/customers/<?php echo $customerinfo["cprofile"];?>" style="width:50px;" alt="">
          <input type="file" name="cprofile" class="form-control my-2">
        </div>
        <div class="form-group">
          <label for="cname">Customer Name:</label>
          <input type="text" name="cname" class="form-control" value="<?php echo $customerinfo["cname"]; ?>">
        </div>
        <div class="form-group">
          <label for="cphone">Customer phone:</label>
          <input type="text" name="cphone" class="form-control" value="<?php echo $customerinfo["cphone"]; ?>">
        </div>
        <div class="form-group">
          <label for="caddress">Customer Address:</label>
          <input type="text" name="caddress" class="form-control" value="<?php echo $customerinfo["caddress"]; ?>">
        </div>
        <div class="form-group">
          <label for="cemail">Customer Email:</label>
          <input type="text" name="cemail" class="form-control" value="<?php echo $customerinfo["cemail"]; ?>">
        </div>
        <div class="form-group">
          <label for="ccityid">Customer City:</label>
          <select name="ccityid" class="form-control">
            <?php
            while ($cityinfo = mysqli_fetch_assoc($cities)) {
             ?>
            <option value="<?php echo $cityinfo["cityid"];?>" <?php if($cityinfo["cityid"]==$customerinfo["ccityid"]) {echo "selected";}?>>
              <?php echo $cityinfo["cityname"];?>
            </option>
            <?php
            }
            ?>
          </select>
        </div>
        <input type="hidden" name="cid" value="<?php echo $customerinfo["cid"]; ?>">
        <button type="submit" class="btn btn-primary btn-lg mx-3">Update</button>
        <a href="Customers.php" class="btn btn-secondary btn-lg">Back</a>
      </form>
    </table>
</div>
</div>

<?php include_once("../layout/footer.php"); ?>