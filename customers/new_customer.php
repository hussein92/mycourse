<?php

include_once("../functions/all.php");
is_logged_in();
$pagetitle ="Add New Customers";


//run add query
if(isset($_POST["cname"])){
$cphoto=$_FILES["cprofile"]["name"];
$cname=$_POST["cname"];
$cphone=$_POST["cphone"];
$cemail=$_POST["cemail"];
$caddress=$_POST["caddress"];
$ccityid=$_POST["ccityid"];
$userid = $_SESSION["userid"];

//Uploading image files
$path="../img/customers/";
$file=$_FILES["cprofile"]["tmp_name"];
move_uploaded_file($file,$path.$cphoto);

addNewCustomer($cphoto,$cname,$cphone,$cemail,$caddress,$ccityid,$userid);

}

$cities = showCities();

include_once("../layout/header.php");
include_once("../layout/navbar.php");
 ?>


<div class="container">
  <h1 class="display-3">New Customer</h1>
  <form action="new_customer.php" method="post" enctype="multipart/form-data">
    <div class="form-group">
      <label for="cprofile">Profile Picture:</label>
      <input type="file" name="cprofile" class="form-control">
    </div>
    <div class="form-group">
      <label for="cname">Customer Name:</label>
      <input type="text" name="cname" class="form-control">
    </div>
    <div class="form-group">
      <label for="cphone">Customer phone:</label>
      <input type="text" name="cphone" class="form-control">
    </div>
    <div class="form-group">
      <label for="caddress">Customer Address:</label>
      <input type="text" name="caddress" class="form-control">
    </div>
    <div class="form-group">
      <label for="cemail">Customer Email:</label>
      <input type="text" name="cemail" class="form-control">
    </div>
    <div class="form-group">
      <label for="ccityid">Customer City:</label>
      <select name="ccityid" class="form-control">
        <?php
        while ($cityinfo = mysqli_fetch_assoc($cities)) {
         ?>
        <option value="<?php echo $cityinfo["cityid"];?>">
          <?php echo $cityinfo["cityname"];?>
        </option>
        <?php
        }
        ?>
      </select>
    </div>
    <button type="submit" class="btn btn-success btn-lg">Save</button>
    <a href="Customers.php" class="btn btn-secondary btn-lg">Back</a>
  </form>
</div>

<?php  include_once("../layout/footer.php"); ?>