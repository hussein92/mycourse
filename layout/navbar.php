<?php if(isset($_SESSION['username'])){$profilename = $_SESSION['username'];} ?>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="index.php"><img src="../img/eda.png" style="width:80px;"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Customers
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="<?php echo $customers; ?>customers.php">Customers List</a>
          <a class="dropdown-item" href="<?php echo $customers; ?>new_customer.php">New Customer</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="<?php echo $customers; ?>search_by_phone.php">Search By Phone</a>
          <a class="dropdown-item" href="<?php echo $customers; ?>search_by_name.php">Search By Name</a>
          <a class="dropdown-item" href="<?php echo $customers; ?>search_by_field.php">Search By Filed</a>
          <a class="dropdown-item" href="<?php echo $customers; ?>search_dynamic.php">Dyanmic Search</a>
        </div>
      </li>
    </ul>
    <ul class="navbar-nav mr-5">
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <?php echo $profilename; ?>
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="<?php echo $users; ?>userprofile.php">Show Profile</a>
          <a class="dropdown-item" href="<?php echo $users; ?>editprofile.php">Edit Profile</a>
          <a class="dropdown-item" href="<?php echo $users; ?>changepassword.php">Change Password</a>
          <a class="dropdown-item" href="<?php echo $users; ?>sign-out.php">Logout</a>
        </div>
      </li>
    </ul>
  </div>
</nav>
