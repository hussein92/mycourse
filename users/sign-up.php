<?php
$pagetitle = " Create a New Account";
include_once "../route.php";///include route page which contan all path
include_once "../functions/basic.php";///contain connection to db
include_once "../functions/users_function.php";


if(isset($_POST["username"])){
  $fn  = $_POST["fname"];
  $ln  = $_POST["lname"];
  $un  = $_POST["username"];
  $pw  = $_POST["password"];
  $cpw = $_POST["confirmpassword"];
  $cid = $_POST["cityid"];

  $users = checkuserexist($un);
  $nou = mysqli_num_rows($users);
  //Check if username is already used
  if($nou > 0){
      //Print an error message if the username is taken
      $error = "This username is already taken";
  } else {
     if($pw == $cpw)
     {
       //Insert a new user if it is NOT
      $newpass = password_hash($pw, PASSWORD_BCRYPT);
      registernewuser($fn, $ln, $un, $newpass, $cid);
    }else{$matcherror ="Password Must Match!!!!!";}

  }
}

//Fetching Cities list
$cities =findcities();
include_once "../layout/header.php";
 ?>

    <div class="container">
      <h1 class="display-2">Create New Account</h1>
      <form action="sign-up.php" method="post">
        <div class="form-group">
          <label for="fname">First Name</label>
          <input type="text" name="fname" id ="fname" class="form-control" value="<?php if(isset($_POST["fname"])){ echo $_POST["fname"]; } ?>">
        </div>
        <div class="form-group">
          <label for="lname">Last Name</label>
          <input type="text" name="lname" id="lname" class="form-control" value="<?php if(isset($_POST["lname"])){ echo $_POST["lname"]; } ?>">
        </div>
        <div class="form-group">
          <label for="username">Username</label>
          <input type="text" name="username" id="username" class="form-control" value="<?php if(isset($_POST["username"])){ echo $_POST["username"]; } ?>">
          <?php if(isset($error)){  ?>
              <p class="alert alert-danger mt-2"><?php echo $error; ?></p>
          <?php } ?>
        </div>
        <div class="form-group">
          <label for="password">Password</label>
          <input type="password" name="password" id="password" class="form-control">
        </div>
        <div class="form-group">
          <label for="confirmpassword">Confirm Password</label>
          <input type="password" name="confirmpassword" id ="confirmpassword" class="form-control">
        <?php if(isset($matcherror)){  ?>
              <p class="alert alert-danger mt-2"><?php echo $matcherror; ?></p>
          <?php } ?>
        </div>
        
        <div class="form-group">
          <label for="cityid">City</label>
          <select class="form-control" name="cityid">
            <?php while($city = mysqli_fetch_assoc($cities)) { ?>
              <option 
              <?php 
                  if(isset($cid) && $cid ==$city['cityid']){echo "selected";}
               ?> 
               value="<?php echo $city["cityid"]; ?>"><?php echo $city["cityname"]; ?></option>
            <?php } ?>
          </select>
        </div>
        <button type="submit" class="btn btn-primary">Create Account</button>
        <button type="reset" class="btn btn-secondary">Clear Form</button>
        <p class="text-center mb-4">Already have an account? <a href="sign-in.php">sign in</a></p>
      </form>
    </div>

<?php include_once "../layout/footer.php"; ?>
