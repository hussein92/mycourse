<?php 
	include_once("../functions/basic.php");
	include_once("../functions/users_function.php");
	is_logged_in();

	$pagetitle =" Edit User Profile";
	$sessionusername = $_SESSION['username'];
	$userProfile = checkuserexist($sessionusername);
	$user =mysqli_fetch_assoc($userProfile);
	$cities =findcities();
	include_once("../layout/header.php");
	include_once("../layout/navbar.php");

	if(isset($_POST["fname"]))
	{
		
		$fname = $_POST["fname"];
		$lname = $_POST["lname"];
		$username = $_POST["username"];
		$cityid = $_POST["cityid"];

		if($sessionusername == $username)
		{
			editProfile($sessionusername,$username,$fname,$lname,$cityid);
		}else
		{
			$users = checkuserexist($username);
			$nou = mysqli_num_rows($users);
			if($nou>0){$error = "This User Is Taken";}
			else{
				$_SESSION['username'] = $username;
				editProfile($sessionusername,$username,$fname,$lname,$cityid);
				}
		}
		
	}
?>

    <div class="container">
      <h1 class="display-2">Edit My Account</h1>
      <form action="editprofile.php" method="POST">
        <div class="form-group">
          <label for="fname">First Name</label>
          <input type="text" name="fname" id ="fname" class="form-control" value=" <?php echo $user['fname']; ?> ">
        </div>
        <div class="form-group">
          <label for="lname">Last Name</label>
          <input type="text" name="lname" id="lname" class="form-control" value=" <?php echo $user['lname']; ?> ">
        </div>
        <div class="form-group">
          <label for="username">Username</label>
          <input type="text" name="username" id="username" class="form-control" value="<?php echo $user['username'];?>">
          <?php if(isset($error)){  ?>
              <p class="alert alert-danger mt-2"><?php echo $error; ?></p>
          <?php } ?>
        </div>
      
        <div class="form-group">
          <label for="cityid">City</label>
          <select class="form-control" name="cityid">
            <?php while($city = mysqli_fetch_assoc($cities)) { ?>
              <option <?php if($city['cityid'] ==$user['cityid']){echo "selected";} ?> value="<?php echo $city["cityid"]; ?>"><?php echo $city["cityname"]; ?></option>
            <?php } ?>
          </select>
        </div>
        <button type="submit" class="btn btn-primary">Save Changes</button>
        <button type="reset" class="btn btn-danger">Clear Form</button>
        <a href="../customers/customers.php" class="btn btn-secondary btn-lg">Back</a>
        
      </form>
    </div>

<?php include_once("../layout/footer.php"); ?>