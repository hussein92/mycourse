<?php 
	include_once("../functions/basic.php");
	include_once("../functions/users_function.php");
	is_logged_in();

	$pagetitle =" Change Password";
	$username = $_SESSION['username'];
	$userProfile = checkuserexist($username);
	$user =mysqli_fetch_assoc($userProfile);
  include_once("../layout/header.php");
  include_once("../layout/navbar.php");


  if(isset($_POST['newpassword']))
  {
    $oldpassword = $_POST["oldpassword"];
    $newpassword = $_POST["newpassword"];
    $confirmnewpassword = $_POST["confirmnewpassword"];

    //$oldhash = password_hash($oldpassword, PASSWORD_BCRYPT);
    if(password_verify($oldpassword,$user['password']))
    {
      if($newpassword == $confirmnewpassword){
        $newpassword = password_hash($newpassword, PASSWORD_BCRYPT);
        changePassword($username,$newpassword);
      }else{$matcherror ="Password Not Match";}
    }else{$matcholdpassword = "Old Password Not Correct";}
  }
?>

    <div class="container">
      <h1 class="display-2">Change Account Password</h1>
      <form action="changepassword.php" method="post">
        <div class="form-group">
          <label for="username">Username</label>
          <input readonly type="text" name="username" id="username" class="form-control" value="<?php echo $user["username"]; ?>">
          <?php if(isset($error)){  ?>
              <p class="alert alert-danger mt-2"><?php echo $error; ?></p>
          <?php } ?>
        </div>
        <div class="form-group">
          <label for="oldpassword">Old Password</label>
          <input type="password" name="oldpassword" id="oldpassword" class="form-control">
          <?php if(isset($matcholdpassword)){  ?>
              <p class="alert alert-danger mt-2"><?php echo $matcholdpassword; ?></p>
          <?php } ?>
        </div>
         <div class="form-group">
          <label for="newpassword">New Password</label>
          <input type="password" name="newpassword" id="newpassword" class="form-control">
        </div>
        <div class="form-group">
          <label for="confirmnewpassword">Confirm New Password</label>
          <input type="password" name="confirmnewpassword" id ="confirmnewpassword" class="form-control">
            <?php if(isset($matcherror)){  ?>
              <p class="alert alert-danger mt-2"><?php echo $matcherror; ?></p>
          <?php } ?>
        </div>
    
        <button type="submit" class="btn btn-primary">Save Change</button>
        <button type="reset" class="btn btn-danger">Clear Form</button>
        <a href="../customers/customers.php" class="btn btn-secondary btn-lg">Back</a>
        
      </form>
    </div>

<?php include_once("../layout/footer.php"); ?>