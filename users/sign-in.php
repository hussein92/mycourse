<?php
$pagetitle= "Sign in to Your Account";
include_once "../route.php";///include route page which contan all path
include_once "../functions/basic.php";///contain connection to db
include_once "../functions/users_function.php";

if (isset($_POST["username"])) {
  $un = $_POST["username"];
  $pw = $_POST["password"];
  $errorstatus = checkuserlogin($un,$pw);
  if($errorstatus == 1)
  {
    $error["username"] = "Username is incorrect";
  }elseif($errorstatus ==2)
  {
      $error["password"] = "Password is incorrect";
  }

}

include_once ("../layout/header.php");///include header here
 ?>

    <div class="container">
      <h1 class="display-2">Sign in</h1>
      <form action="sign-in.php" method="post">
        <div class="form-group">
          <label for="username">Username</label>
          <input type="text" name="username" id ="username" class="form-control">
          <?php if(isset($error["username"])){ ?>
            <p class="alert alert-danger mt-2"><?php echo $error["username"]; ?></p>
          <?php } ?>
        </div>
        <div class="form-group">
          <label for="password">Password</label>
          <input type="password" name="password" id ="password" class="form-control">
          <?php if(isset($error["password"])){ ?>
            <p class="alert alert-danger mt-2"><?php echo $error["password"]; ?></p>
          <?php } ?>
        </div>
        <button type="submit" class="btn btn-success">Sign in</button>
        <button type="reset" class="btn btn-secondary">Clear Form</button>
        <p class="text-center mb-4">Don't have an account? <a href="sign-up.php">Create account.</a></p>
      </form>
    </div>

<?php include_once ("../layout/footer.php");///include footer here ?>
