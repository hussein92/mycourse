<?php

// function to show active customers with its cities name  or not active according to status paramater 
function show_customers($status){
  global $conn;
  $customersel="SELECT customers.*, cities.cityname FROM customers, cities WHERE cities.cityid=customers.ccityid AND customers.active= '$status'";
  $cust_set = mysqli_query($conn,$customersel);
  return $cust_set;
}

// functtion to delete or restore customer that according to action paramater 
function delete_restore_customer($cid, $action){
  global $conn;
  $customerdel="UPDATE customers SET active='$action' WHERE cid='$cid'";
  mysqli_query($conn,$customerdel);
  if ($action == 0) {
    header("Location: customers.php");
  }
  elseif ($action == 1) {
      header("Location: customers_deleted.php");
  }
}

// Function To Add New Customer To Db

function addNewCustomer($cphoto,$cname,$cphone,$cemail,$caddress,$ccityid,$userid)
{
  global $conn;
  $sql = "INSERT INTO customers (cprofile,cname,cphone,cemail,caddress,ccityid,userid) VALUES ('$cphoto','$cname','$cphone','$cemail','$caddress','$ccityid','$userid')";
  if(mysqli_query($conn,$sql))
  {
    header("location: customers.php");
  }else{}
}


// function to show all cities

   function showCities()
   {
      global $conn;
      $sql = "SELECT * FROM cities;";
      return $cities = mysqli_query($conn,$sql);

   }

/// start search functions
// 1-search by phone
// 2-search by name  
// 3-search by any chosen one filed combobox [name.phone,email,address,city]
// 4-search Dynamicaly in all fields [name.phone,email,address,city]

   function customersSearch($search,$method,$searchType)
   {
      $search = trim($search);
      global $conn;
      //search by matching part of word 
      if($searchType =="part")
      {
        if($method == "dynamic")
        {
          $sql =
          "SELECT customers.*, cities.cityname FROM customers, cities WHERE cities.cityid=customers.ccityid AND ( customers.cname LIKE '%$search%' OR customers.cphone LIKE '%$search%' OR customers.cemail LIKE '%$search%' OR customers.caddress LIKE '%$search%' OR cities.cityname LIKE '%$search%')";
            return $cust_set =mysqli_query($conn,$sql);

        }else
        {
          $sql ="SELECT customers.*, cities.cityname FROM customers, cities WHERE cities.cityid=customers.ccityid AND $method LIKE '%$search%'";
           return $cust_set =mysqli_query($conn,$sql);
        }
        
         //search by matching full  word 
      }else if($searchType =="full")
      {
        if($method == "dynamic")
        {
          $sql =
          "SELECT customers.*, cities.cityname FROM customers, cities WHERE cities.cityid=customers.ccityid AND ( customers.cname = '%$search%' OR customers.cphone = '%$search%' OR customers.cemail = '%$search%' OR customers.caddress = '%$search%' OR cities.cityname = '%$search%')";
            return $cust_set =mysqli_query($conn,$sql);

        }else
        {
          $sql ="SELECT customers.*, cities.cityname FROM customers, cities WHERE cities.cityid=customers.ccityid AND $method='$search'";
          return $cust_set =mysqli_query($conn,$sql); 
        }          
      } 
   }//end search function


// function to show certain customer
   function showCustomer($id)
   {
    global $conn;
    $sql = "SELECT customers.*,cities.cityname FROM customers,cities WHERE customers.ccityid = cities.cityid AND customers.cid = '$id'";
    return mysqli_query($conn,$sql);
   }


   // function to update customer

   function updateCustomer($photoQuery,$cname,$cphone,$cemail,$caddress,$ccityid,$cid)
   {
    global $conn;
    $sql ="UPDATE customers SET cname='$cname',cphone='$cphone',cemail='$cemail',caddress='$caddress',ccityid='$ccityid' $photoQuery  WHERE cid='$cid'";

    mysqli_query($conn,$sql);
    header("location: customers.php");
   }
 ?>
