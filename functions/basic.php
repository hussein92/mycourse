<?php
include "../route.php";
session_start();
$conn = mysqli_connect('localhost','root','','eda');


// function to check is authorized or not
function is_logged_in(){
  if(!isset($_SESSION["username"]) || empty($_SESSION["username"])){
    header("Location: /session/users/sign-in.php");
  }
}


// function to clean input data from user
function cleanInputData($input)
{
  global $conn;
  $cleaned = mysqli_real_escape_string($conn,trim($input));
  return $cleaned;
}


 ?>
